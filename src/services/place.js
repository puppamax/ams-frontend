import axios from 'axios';

export default (req) => {
    /*
        new class promise for axios return data from api
     */
    const promise = new Promise((resolve, reject) => {
        /*
         *  keep data to localStorage
         */


        let data = localStorage.getItem(req)
        if (data) {
            /*
                axios call to api
             */
            axios
                .get('http://localhost:8000/api/place', { params :req })
                .then((res) => {
                    // success callback
                    if(res.data.error_message){
                        reject(res.data.error_message)
                    }else{
                        localStorage.setItem(req, JSON.stringify(res.data.results))
                        resolve(res.data.results)
                    }
                })
                .catch(error => {
                    reject(error)
                })
        } else {
            resolve(JSON.parse(data))

        }
    })
    return promise
}